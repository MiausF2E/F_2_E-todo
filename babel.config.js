module.exports = {
  presets: [
    ['@babel/preset-env', {
      modules: false,
      useBuiltIns: 'usage'
    }],
    ['@babel/preset-stage-2', {
      decoratorsLegacy: true
    }]
  ],
  plugins: [
    'transform-vue-jsx',
    '@babel/plugin-transform-modules-commonjs',
    ['transform-imports', {
      vuetify: {
        transform: 'vuetify/es5/components/${member}',
        preventFullImport: true
      }
    }]
  ],
  env: {
    test: {
      presets: [
        '@babel/preset-env',
        ['@babel/preset-stage-2', {
          decoratorsLegacy: true
        }]
      ],
      plugins: [
        'transform-vue-jsx',
        '@babel/plugin-transform-modules-commonjs',
        'dynamic-import-node'
      ]
    }
  }
}
