/* Note: it can be split into multi modules, but it's no need for the simple todo app. */

import Vue from 'vue'

const defaults = {
  tasks: []
}

const preset = {
  tasks: [
    {
      comment: 'meet him at Lorence Cafe',
      completed: false,
      date: '2018-05-14',
      files: [{ filename: '00.jpg', createTime: 1528444658550 }],
      starred: true,
      title: 'Cloud project with Dannie',
      time: '14:30',
      uuid: '6d0e460a-6ff4-4ee7-a8f6-f742705b7887'
    },
    {
      comment: 'WOW',
      completed: false,
      date: null,
      files: [{ filename: 'GBF4A.jpg', createTime: 1528445012084 }],
      starred: true,
      title: 'Second job',
      time: null,
      uuid: '22c6d4ee-6568-49ed-87bc-b586509a331e'
    },
    {
      comment: '',
      completed: false,
      date: '2018-06-18',
      files: [],
      starred: false,
      title: 'only date',
      time: null,
      uuid: 'af55122a-fbea-4e32-aa87-7281101cf8e1'
    },
    {
      comment: '',
      completed: false,
      date: null,
      files: [{ filename: 'GBF4A.jpg', createTime: 1528445310270 }],
      starred: false,
      title: 'only file',
      time: null,
      uuid: '2ba9fa61-a544-4137-8bc5-53fc85d0d305'
    },
    {
      comment: '',
      completed: true,
      date: null,
      files: [],
      starred: false,
      title: 'Job done',
      time: null,
      uuid: '46fcd75f-3ff5-4f31-9f12-dd7f4580d927'
    }
  ]
}

const mutations = {
  AddTask (state, data) {
    state.tasks.push(data)
  },
  DeleteTask (state, uuid) {
    Vue.set(state, 'tasks', state.tasks.filter(task => task.uuid !== uuid))
  },
  EditTask (state, data) {
    state.tasks.some((task, index) => {
      if (data.uuid === task.uuid) {
        Vue.set(state.tasks, index, data)
        return true
      }
    })
  },
  LoadPreset (state) {
    Object.keys(preset).forEach(key => Vue.set(state, key, preset[key]))
  },
  UpdateSort (state, tasks) {
    Vue.set(state, 'tasks', tasks)
  }
}

const getters = {
  leftTask (state) {
    return state.tasks.filter(task => !task.completed).length
  },
  completedTask (state) {
    return state.tasks.filter(task => task.completed).length
  }
}

export default {
  state: defaults,
  mutations,
  getters
}
