import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/src/stylus/main.styl'

import Vue from 'vue'
import store from './store'
import App from './App.vue'
import {
  Vuetify,
  VApp,
  VBtn,
  VDatePicker,
  VGrid,
  VIcon,
  VMenu,
  VTabs,
  VTextarea,
  VTextField,
  VTimePicker,
  transitions
} from 'vuetify'
import { Ripple } from 'vuetify/es5/directives'

Vue.config.productionTip = false

Vue.use(Vuetify, {
  components: {
    VApp,
    VBtn,
    VDatePicker,
    VGrid,
    VIcon,
    VMenu,
    VTabs,
    VTextarea,
    VTextField,
    VTimePicker,
    transitions
  },
  directives: {
    Ripple
  },
  iconfont: 'mdi'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
